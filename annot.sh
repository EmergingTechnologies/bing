#!/bin/bash


THIS_IMAGE_URI="$1"
THIS_TEXT_FOR_BING="$2"

# convert  -font '文泉驿微米黑' \
#     bingbgs/'GrizzlyPeak_ZH-CN10461951049_1366x768.jpg' \
#     -background black  label:'Faerie Dragon' \
#     +swap  -gravity Center -append  \
#     bingbgs/anno_fancy.jpg


convert -font '方正行楷繁体' -pointsize 18 \
    -size 800x34 xc:none -gravity east \
    -stroke black -strokewidth 2 -annotate 0 "$THIS_TEXT_FOR_BING" \
    -background none -shadow 100x3+0+0 +repage \
    -stroke none -fill white     -annotate 0 "$THIS_TEXT_FOR_BING" \
    "$THIS_IMAGE_URI" \
    +swap -gravity northeast -geometry +0-3 \
    -composite  \
    "$THIS_IMAGE_URI"
    # bingbgs/annot.jpg

# convert -size 100x14 xc:none -gravity northeast \
#     -font '文泉驿微米黑' \
#     -stroke black -strokewidth 2 -annotate 0 '中文字符' \
#     -background none -shadow 100x3+0+0 +repage \
#     -stroke none -fill white     -annotate 0 '中文字符' \
#     bingbgs/'GrizzlyPeak_ZH-CN10461951049_1366x768.jpg'  +swap -gravity northeast -geometry +0-3 \
#     -composite  bingbgs/anno_fancy.jpg
