(defproject bing "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [http-kit "2.2.0"]
                 [hiccup "1.0.5"]
                 [hickory "0.7.0"]
                 [cheshire "5.7.0"]]
  :profiles {:uberjar {:aot [bing.core]}}
  :main bing.core)
